import problem1 from "../problem1.js"
import inventory from "../inventory.js"

const result = problem1(inventory, 33);
const result2 = problem1([]);

console.log(result);
console.log(result2);
