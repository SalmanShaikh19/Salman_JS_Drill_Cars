function problem6(inventory) {
    if (!inventory || inventory.length === 0) {
        return ("Pls provide inventory");
    }

    const BMW_Audi = [];

    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].car_make === "BMW" || inventory[index].car_make === "Audi") {
            BMW_Audi.push(inventory[index]);
        }
    }

    const myJson = JSON.stringify(BMW_Audi);
    return myJson;
}

export default problem6;