const problem3 = (inventory) => {
    if (!inventory || inventory.length === 0) {
        return ("Pls provide inventory");
    }

    const car_models = [];

    for (let index = 0; index < inventory.length; index++) {
        car_models.push(inventory[index].car_model);
    }

    const result = car_models.sort();
    return result;
}

export default problem3;