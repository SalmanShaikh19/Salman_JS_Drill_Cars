// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function problem5(car_years) {
    if(!car_years || car_years.length === 0){
        return("Please provide car_years array")
    }

    const result = [];

    for (let index = 0; index < car_years.length; index++) {
        if (car_years[index] < 2000) {
            result.push(car_years[index]);
        }
    }

    return result;
}

export default problem5;