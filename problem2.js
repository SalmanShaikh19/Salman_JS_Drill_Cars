// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
//"Last car is a *car make goes here* *car model goes here*"

const problem2 = (inventory) => {
    if (!inventory || inventory.length === 0) {
        return ("Pls provide inventory");
    }

    let lastIndex = inventory.length - 1;

    return (`Last car is a ${inventory[lastIndex].car_make} ${inventory[lastIndex].car_model}`);
}

export default problem2;